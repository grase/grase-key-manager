-- #use vpnkeys;

--
-- Table structure for table `certs`
--

DROP TABLE IF EXISTS `certs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certs` (
  `certID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fingerprint` varchar(32) NOT NULL,
  `cn` varchar(200) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `csr` varchar(4300) NOT NULL,
  `crt` varchar(8600) DEFAULT NULL,
  PRIMARY KEY (`certID`),
  UNIQUE KEY `fingerprint` (`fingerprint`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='CSRs and CRTs';

--
-- Table structure for table `keylogs`
--

DROP TABLE IF EXISTS `keylogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keylogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fingerprint` varchar(32) NOT NULL,
  `remoteip` varchar(46) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `ip` (`remoteip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Logs of key access';

