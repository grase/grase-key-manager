<?php

namespace App;

class CA
{

    private $caCertificate;
    private $caKey;
    private $projectDir;

    private $signedDays = 400;

    public function __construct($caCertificate, $caKey, $projectDir)
    {
        $this->caCertificate = $caCertificate;
        $this->caKey         = $caKey;
        $this->projectDir = $projectDir;
    }

    public function hasCertificateExpired($certificate)
    {
        $certificateString = escapeshellarg($certificate);
        exec("echo $certificateString |  openssl x509 -noout -checkend 0", $output, $returnval);
        if ($returnval != 0) {
            return true;
        } // Key has expired

        return false;
    }

    public function getCsrFingerprint($csr)
    {
        $csrString   = escapeshellarg($csr);
        $fingerprint = trim(shell_exec("echo $csrString |  openssl req -noout -modulus |openssl md5"));
        list($dummy, $fingerprint) = @ explode("=", $fingerprint, 2);
        if ( ! $fingerprint) {
            $fingerprint = $dummy;
        }
        $fingerprint = trim($fingerprint);
        if (strlen($fingerprint) != 32) {
            throw new \Exception('Invalid fingerprint ' . $fingerprint);
        }

        return $fingerprint;
    }

    public function signCsr($csr)
    {

        $configArgs = [
            'config'          => $this->projectDir . '/openssl.cnf',
            'digest_alg'      => 'sha1',
            "x509_extensions" => "usr_cert",
        ];
        $signedCertificate = openssl_csr_sign($csr, $this->caCertificate, $this->caKey, $this->signedDays, $configArgs);

        if (!$signedCertificate) {
            throw new \Exception("Problem signing CSR");
        }

        if ( ! openssl_x509_export($signedCertificate, $cert)) {
            throw new \Exception('Unable to sign CSR ' . $csr);
        }

        return $cert;
    }
}