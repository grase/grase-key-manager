<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CertificateRepository")
 */
class Certificate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=44)
     * Valid Fingerprint should be 32 long, we invalidate it with _timestamp making it 43 long
     */
    private $fingerprint;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $cn;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="string", length=4300)
     */
    private $csr;

    /**
     * @ORM\Column(type="string", length=8600, nullable=true)
     */
    private $crt;

    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    public function setFingerprint(string $fingerprint): self
    {
        $this->fingerprint = $fingerprint;

        return $this;
    }

    /**
     * @return string
     */
    public function getCn()
    {
        return $this->cn;
    }

    public function setCn(string $cn): self
    {
        $this->cn = $cn;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return string
     */
    public function getCsr()
    {
        return $this->csr;
    }

    public function setCsr(string $csr): self
    {
        $this->csr = $csr;

        return $this;
    }

    /**
     * @return string
     */
    public function getCrt()
    {
        return $this->crt;
    }

    public function setCrt(string $crt): self
    {
        $this->crt = $crt;

        return $this;
    }
}
