<?php

namespace App\Controller;

use App\CA;
use App\Entity\Certificate;
use App\Entity\Logs;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class CertificateController extends Controller
{
    /** @var CA */
    private $CA;

    public function __construct(KernelInterface $kernel)
    {
        // @TODO move these to config param
        $caCertificate = getenv('CA_CERT');
        $caKey = getenv('CA_KEY');

        $this->CA = new CA($caCertificate, $caKey, $kernel->getProjectDir());
    }

    /**
     * @Route("/getcrt.php")
     * @Route("/getCertificate", name="certificate")
     */
    public function getCertificate(Request $request, LoggerInterface $logger)
    {
        $signingRequest = $request->request->get('csr');
        if (strlen($signingRequest) < 100) {
            $logger->info("Missing CSR", $request->request->all());
            return $this->json([
                'status' => 500,
                'message' => 'Missing CSR'
            ], 500);
        }

        // Get Fingerprint
        $fingerprint = $this->CA->getCsrFingerprint($signingRequest);

        // log request
        $this->logRequest($fingerprint, $request->getClientIp());

        // Check if fingerprint is in DB
        $certificateRepo = $this->getDoctrine()->getManager()->getRepository(Certificate::class);

        $certificate = $certificateRepo->findOneBy(['fingerprint' => $fingerprint]);

        // If no certificate exists, create and return new certificate
        if (!$certificate) {
            $certificate = $this->newCertificate($signingRequest, $fingerprint);
        }

        // If Certificate has expired, invalidate it and generate a new one
        if ($this->CA->hasCertificateExpired($certificate->getCrt())) {
            // Invalidate old record
            $invalidatedFingerprint = $certificate->getFingerprint() . "_" . time();
            $certificate->setFingerprint($invalidatedFingerprint);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($certificate);
            $entityManager->flush();

            // Generate new certificate
            $certificate = $this->newCertificate($signingRequest, $fingerprint);

        }

        return new Response($certificate->getCrt());
    }

    /**
     *
     * Takes a CSR & Fingerprint, signs and returns the Certificate
     *
     * @param $signingRequest
     * @param $fingerprint
     *
     * @return Certificate
     * @throws \Exception
     */
    private function newCertificate($signingRequest, $fingerprint) {

        // Get CN
        $details = openssl_csr_get_subject($signingRequest);
        $commonName = $details['CN'];

        $certificate = new Certificate();

        $certificate->setCn($commonName);
        $certificate->setCsr($signingRequest);
        $certificate->setFingerprint($fingerprint);

        $crt = $this->CA->signCsr($signingRequest);

        $certificate->setCrt($crt);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($certificate);
        $entityManager->flush();

        return $certificate;
    }

    private function logRequest($fingerprint, $ip)
    {
        $log = new Logs();
        $log->setFingerprint($fingerprint);
        $log->setRemoteIP($ip);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($log);
        $entityManager->flush();
    }
}
